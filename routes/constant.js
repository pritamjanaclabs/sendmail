/**
 * Created by karan on 27-Feb-15.
 */
function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value:        value,
        enumerable:   true,
        writable:     false,
        configurable: false
    });
}

exports.responseStatus = {};
define(exports.responseStatus, "PARAMETER_MISSING", 100);
define(exports.responseStatus, "SHOW_MESSAGE", 104);
define(exports.responseStatus, "SHOW_ERROR_MESSAGE", 103);

exports.responseMessage = {};
define(exports.responseMessage, "PARAMETER_MISSING", "Some Parameters Missing");
define(exports.responseMessage, "SHOW_MESSAGE", "Successfully Sent your EMail!");
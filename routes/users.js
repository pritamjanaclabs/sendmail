/**
 * Created by karan on 27-Feb-15.
 */
var express = require('express');
var router = express.Router();
var commonFunc=require('./commonFunction');
var sendResponse = require('./sendResponse');
var async = require('async');
var request = require('request');

router.post('/register_user',function(req,res){
    var userMail = req.body.email;
    var sub =req.body.subject;
    var message =req.body.message;
    var manValues = [userMail,sub,message];
    async.waterfall([
            function (callback) {
                commonFunc.checkBlank(res, manValues, callback);
            },
            function (callback){
                checkValidMail(res,userMail,callback);
            }],function(err,resultCallback){
                commonFunc.sendEmail(userMail, sub, message, function (result) {
                    if (result == 1) {
                        sendResponse.successStatusMsg(res);
                        console.log("Mail Sent");
                    }
                    else {
                        sendResponse.wrongMailId(res);
                        console.log("Error Sending Mail");
                        //res.send("Error Sending Mail");
                    }
                })
            }
    );

});
function checkValidMail(ress,userMail,callback){
    request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: userMail},
        json: true
    }, function (err,res, body) {
        //console.log(body.valid);
        if (body.valid) {
            callback (null);
        }
        else {
            sendResponse.wrongMailId(ress);
            console.log(userMail);
            console.log("Email not Exist On SMTP Server");
        }
    });
}
router.post('/login_user',function(req,res){
    res.send("Under construction");
});
module.exports = router;
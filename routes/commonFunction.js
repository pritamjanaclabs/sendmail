/**
 * Created by karan on 27-Feb-15.
 */
var mailConfig=config.get('emailSettings');
var sendResponse = require('./sendResponse');

exports.sendEmail = function(receiverMailId, subject,message, callback) {
    var nodemailer = require("nodemailer");
    var smtpTransport = nodemailer.createTransport("SMTP", {
        service: 'Gmail',
        auth: {
            user: mailConfig.email,
            pass: mailConfig.password
        }
    });

    var mailOptions = {
        from: mailConfig.email,
        to: receiverMailId,
        subject: subject,
        text: 'WellCome To ------',
        html: '<b>'+message+'</b>'

    }
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            return callback(0);
        } else {
            //  console.log('hi');
            return callback(1);
        }
    });
};

exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {

    var arrLength = arr.length;

    for (var i = 0; i < arrLength; i++) {
        if ((arr[i] == '')||(arr[i] == undefined)||(arr[i] == '(null)')) {
            return 1;
            break;
        }
    }
    return 0;
}